FROM docker:20.10.1

RUN apk add --update --no-cache jq py-pip sed
RUN pip install awscli
